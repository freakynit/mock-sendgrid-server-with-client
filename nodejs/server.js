var PORT = 9002;

var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var url = require('url');
var open = require("open");

var path = require('path');
var engine = require('ejs-mate');

global.appRoot = path.resolve(__dirname);

var app = express();

// app.use(function(req, res, next) {
//     if(req.url.indexOf("/local_with_upload") > -1) {
//         req.rawBody = '';
//         req.on('data', function(chunk) {
//             req.rawBody += chunk;
//         });
//         req.on('end', function() {
//             require('./routes/api/local_with_upload').router.handleEventsData(req, res);
//         });
//     } else {
//         next();
//     }
// });

app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(favicon(__dirname + '/public/images/favicon.png'));
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({
    limit: '50mb', extended: true
}));
app.use(bodyParser.json({limit: '50mb'}));

var router = express.Router();

app.use('/api/mail.send.json', require('./routes/mail.send.json.js').router);
app.use('/', require('./routes/index').router);

var server = app.listen(PORT, function() {
    console.log("Listening to port %s", server.address().port);
});

// open('http://localhost:' + PORT + '/api/mail.send.json/stats', function (err) {
//   // if (err) throw err;
//   // console.log('The user closed the browser');
// });
