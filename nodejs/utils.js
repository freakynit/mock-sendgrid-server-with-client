module.exports = {
	isPayloadValid: function(formPayload){
		if(formPayload['to'] === undefined || formPayload['subject'] === undefined || formPayload['from'] === undefined) {
			return false;
		} else {
			return true;
		}
	}
};
