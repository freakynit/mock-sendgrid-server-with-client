Handlebars.getTemplate = function(name, relativePath, cb) {
    if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
        $.ajax({
            url: '/templates/' + relativePath + name + '.handlebars',
            success: function(data) {
                if (Handlebars.templates === undefined) {
                    Handlebars.templates = {};
                }
                var compiled = Handlebars.compile(data);
                Handlebars.templates[name] = compiled;
                cb(Handlebars.templates[name]);
            }/*,
            async: false*/
        });
    } else {
        cb(Handlebars.templates[name]);
    }
    //return Handlebars.templates[name];
};
