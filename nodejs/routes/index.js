// requires
var express = require('express');
var router = express.Router();

router.get('/', function(req, res){
    res.render('index/index', {title: 'SendGrid Mock Server Web UI'});
});

module.exports.router = router;
