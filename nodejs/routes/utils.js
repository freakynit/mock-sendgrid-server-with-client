var REAL_MAIL_SENDER_STRING = '"fa80418" <fa80418@gmail.com>';
var REAL_MAIL_TRANSPORT_STRING = 'smtps://fa80418%40gmail.com:admin123A@@smtp.gmail.com';

var URL_MATCH_REGEX = /(^|[\s\n]|<br\/?>)((?:https?|ftp):\/\/[\-A-Z0-9+\u0026\u2019@#\/%?=()~_|!:,.;]*[\-A-Z0-9+\u0026@#\/%=~()_|])/gi;

var randomip = require('random-ip');
var nodemailer = require('nodemailer');

var cl = function(msg){
    console.log(msg);
};

var userAgents = [
	"Mozilla/5.0 (Linux; Android 5.1.1; SM-G928X Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36",
	"Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 950) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Mobile Safari/537.36 Edge/13.10586",
	"Mozilla/5.0 (Linux; Android 6.0.1; Nexus 6P Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36",
	"Mozilla/5.0 (Linux; Android 5.0.2; SAMSUNG SM-T550 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/3.3 Chrome/38.0.2125.102 Safari/537.36",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9",
	"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36",
	"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1",
	"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)",
	"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
	"Mozilla/5.0 (Macintosh; U; PPC Mac OS X; fi-fi) AppleWebKit/420+ (KHTML, like Gecko) Safari/419.3",
	"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1b3) Gecko/20090305 Firefox/3.1b3 GTB5",
	"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.4 (KHTML, like Gecko) Chrome/6.0.481.0 Safari/534.4"
];

var bounceReasons = [
	"500 unknown recipient",
	"User not found",
	"Some other reason"
];

var dropReasons = [
	"Bounced Address",
	"Address not found",
	"Some other reason"
];

var utils = {
	maxErrorRate: 0.2,
	transporter: null,
	webhookEventDetails: {
		"processed": {
			"getProbability": function(){
				return 1;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				
			},
			"eventType": "delivery",
			"scheduleAt": function(){
				return new Date(new Date().getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		},

		"dropped": {
			"getProbability": function(){
				return 0.01;
			},
			"marksEnd": true,
			"retry": {
				"capable": false
			},
			"data": {
				"reason": function(){
					return dropReasons[utils.getRandomNumber(0, dropReasons.length)];
				},
	    		"status": "5.0.0"
			},
			"eventType": "delivery",
			"scheduleAt": function(){
				var curdate = new Date();
				return new Date(curdate.getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		},
		
		"deferred": {
			"getProbability": function(){
				return 0.01;
			},
			"marksEnd": false,
			"retry": {
				"capable": true,
				"getProbability": function(){
					return 0.1;
				}
			},
			"data": {
				"response": "400 try again later",
				"attempt": function(){
					return utils.getRandomNumber(0, 5).toString();
				}	
			},
			"eventType": "delivery",
			"scheduleAt": function(){
				var curdate = new Date();
				return new Date(curdate.getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		},

		"bounce": {
			"getProbability": function(){
				return 0.02;
			},
			"marksEnd": true,
			"retry": {
				"capable": false
			},
			"data": {
				"reason": function(){
					return bounceReasons[utils.getRandomNumber(0, bounceReasons.length)];
				},
				"status": "5.0.0"	
			},
			"eventType": "delivery",
			"scheduleAt": function(){
				var curdate = new Date();
				return new Date(curdate.getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		},
		
		"delivered": {
			"getProbability": function(){
				return 1;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				"response": "250 OK"	
			},
			"eventType": "delivery",
			"scheduleAt": function(){
				var curdate = new Date();
				return new Date(curdate.getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		},

		"open": {
			"getProbability": function(){
				return 0.1;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				
			},
			"eventType": "engagement",
			"scheduleAt": function(){
				var curdate = new Date();
				return new Date(curdate.getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		},

		"click": {
			"getProbability": function(){
				return 0.1;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				
			},
			"eventType": "engagement",
			"scheduleAt": function(){
				var curdate = new Date();
				return new Date(curdate.getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		},

		"spamreport": {
			"getProbability": function(){
				return 0.05;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				
			},
			"eventType": "engagement",
			"scheduleAt": function(){
				var curdate = new Date();
				return new Date(curdate.getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		},

		"unsubscribe": {
			"getProbability": function(){
				return 0.025;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				
			},
			"eventType": "engagement",
			"scheduleAt": function(){
				var curdate = new Date();
				return new Date(curdate.getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		},

		"group_unsubscribe": {
			"getProbability": function(){
				return 0.05;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				"asm_group_id": function(){
					return utils.getRandomNumber(0, 10);
				}	
			},
			"eventType": "engagement",
			"scheduleAt": function(){
				var curdate = new Date();
				return new Date(curdate.getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		},

		"group_resubscribe": {
			"getProbability": function(){
				return 0.001;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				"asm_group_id": function(){
					return utils.getRandomNumber(0, 10);
				}	
			},
			"eventType": "engagement",
			"scheduleAt": function(){
				var curdate = new Date();
				return new Date(curdate.getTime() + utils.getRandomNumber(2, 10) * 1000);
			}
		}
	},

	webhookEvents: [],

	init: function(){
		this.transporter = nodemailer.createTransport(REAL_MAIL_TRANSPORT_STRING);

		for(key in this.webhookEventDetails){
			if(this.webhookEventDetails.hasOwnProperty(key)) {
				this.webhookEvents.push(key);
			}
		}
	},

	getResponseJson: function(formPayload){
		var errors = [];

		if(formPayload['from'] === undefined || formPayload['from'].length < 1) {
			errors.push('Empty from email address (required)');
		}

		if(formPayload['subject'] === undefined || formPayload['subject'].length < 1) {
			errors.push('Missing subject');
		}

		if((formPayload['text'] === undefined || formPayload['text'].length < 1) 
			&& (formPayload['html'] === undefined || formPayload['html'].length < 1)) {
			errors.push('Missing email body');
		}

		if(formPayload['toname'] !== undefined && formPayload['toname'].length != formPayload['to'].length) {
			errors.push('Parameters to and toname should be the same length');
		}

		if(errors.length > 0) {
			return {
				"errors": errors,
				"message": "error"
			};
		} else {
			return {
				"message": "success"
			};
		}
	},

	setErrorRate: function(errRate){
		this.maxErrorRate = errRate;
	},

	getRandomProbability: function(){
		return Math.random();
	},

	getRandomNumber: function(min, max){
		return Math.floor(Math.random() * max) + min;
	},

	mockErrorInFormPayload: function(formPayload){
		if(this.maxErrorRate > 0) {
			if(this.maxErrorRate >= this.getRandomProbability()) {
				return this.introduceErrorInFormPayload(formPayload);
			} else {
				return formPayload;
			}
		} else {
			return formPayload;
		}
	},

	randomString: function(length){
		return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
	},

	introduceErrorInFormPayload: function(formPayload){
		var curErrorProbabilityThreshold = this.getRandomProbability();

		if(curErrorProbabilityThreshold < 0.3) {
			delete formPayload["from"];
		} else if(curErrorProbabilityThreshold >= 0.3 && curErrorProbabilityThreshold < 0.6) {
			delete formPayload["subject"];
		} else if(curErrorProbabilityThreshold >= 0.6 && curErrorProbabilityThreshold < 0.9) {
			delete formPayload["text"];
			delete formPayload["html"];
		} else {
			if(formPayload["toname"] && formPayload["toname"].length > 0) {
				formPayload["toname"].splice(0, 1);
			}
		}

		return formPayload;
	},

	getWebhookResponseDetails: function(webhookJobPayload, emailId, emailName){
		var webhookResponseDetails = {
			"payload": null,
			"stage": "end",
			"scheduleAt": null
		};

		if(webhookJobPayload.stage == null || webhookJobPayload.stage === undefined || webhookJobPayload.stage.length < 1) {
			cl("Undefined webhookJobPayload.stage");
			return webhookResponseDetails;
		}
	
		var curStageEventName = webhookJobPayload.stage;
		var curStageEventDetails = this.webhookEventDetails[curStageEventName];

		var nextEventIndex = this.webhookEvents.indexOf(curStageEventName) + 1;
		if(nextEventIndex < this.webhookEvents.length) {
			var nextStageEventName = this.webhookEvents[nextEventIndex];
			webhookResponseDetails["scheduleAt"] = curStageEventDetails.scheduleAt();
			
			if(curStageEventDetails.getProbability() >= this.getRandomProbability()) {
				webhookResponseDetails["payload"] = this.generateRandomDataForWebhookEvent(webhookJobPayload, emailId, curStageEventName, curStageEventDetails.eventType);

				if(curStageEventDetails.marksEnd == false) {
					if(curStageEventDetails.retry.capable == true
						&& (curStageEventDetails.retry.getProbability() >= this.getRandomProbability())) {
						webhookResponseDetails.stage = curStageEventName;
					} else {
						webhookResponseDetails.stage = nextStageEventName;
					}
				} else {
					webhookResponseDetails.stage = "end";		
				}
			} else {
				webhookResponseDetails["payload"] = null;
				webhookResponseDetails.stage = nextStageEventName;
			}
		} else {
			webhookResponseDetails["payload"] = null;
			webhookResponseDetails.stage = "end";
		}

		return webhookResponseDetails;
	},

	generateRandomDataForWebhookEvent: function(webhookJobPayload, emailId, eventName, eventType){
		var responseData = {
			"email": emailId,
			"timestamp": Math.floor(new Date().getTime() / 1000),
			"event": eventName,
			"sg_event_id": this.randomString(20),
			"sg_message_id": this.randomString(50),
			"ip": randomip('0.0.0.0'),
			"tls": false,
			"cert_err": false
		};

		if(eventType == "engagement") {
			responseData["useragent"] = userAgents[utils.getRandomNumber(0, userAgents.length)];
			responseData["ip"] = randomip('0.0.0.0');

			var link = null;
			if(webhookJobPayload.links && webhookJobPayload.links.length > 0) {
				link = webhookJobPayload.links[utils.getRandomNumber(0, webhookJobPayload.links.length)];
			} else {
				link = "http://sendgrid.com/";
			}
			responseData['url'] = link;
		}

		// Add SMTP data
		if(webhookJobPayload['x-smtpapi']) {
			var xSmtpapiDetails = webhookJobPayload['x-smtpapi'];
			if(xSmtpapiDetails.unique_args && Object.prototype.toString.call(xSmtpapiDetails.unique_args) === '[object Object]') {
				for(var key in xSmtpapiDetails.unique_args){
					responseData[key] = xSmtpapiDetails.unique_args[key];
				}
			}

			var category = null;
			if(xSmtpapiDetails.category) {
				if(Object.prototype.toString.call(xSmtpapiDetails.category) === '[object Array]') {
					category = [];
					for(var i = 0; i < xSmtpapiDetails.category.length; i++){
						category.push(xSmtpapiDetails.category[i]);
					}
				} else {
					category = xSmtpapiDetails.category;
				}
			}
			if(category != null) {
				responseData[category] = category;
			}
		}

		var eventExtraData = this.webhookEventDetails[eventName].data;
		for(var key in eventExtraData){
			if(eventExtraData.hasOwnProperty(key)) {
				var val = null;
				if(Object.prototype.toString.call(eventExtraData[key], webhookJobPayload.links) == '[object Function]') {
					val = eventExtraData[key].call(webhookJobPayload.links);
				} else {
					val = eventExtraData[key];
				}

				responseData[key] = val;
			}
		}

		return responseData;
	},

	sendMail: function(mailData, cb){
		var mailOptions = {
		    from: REAL_MAIL_SENDER_STRING,
		    to: mailData.to,
		    subject: mailData.subject,
		    text: mailData.text,
		    html: mailData.html
		};

		this.transporter.sendMail(mailOptions, cb);
	},

	clone: function(obj){
		return JSON.parse(JSON.stringify(obj));
	},

	constructWebhookJobPayload: function(formPayload){
		var webhookJobPayloads = [];

		var tos = formPayload.to;
		if(Object.prototype.toString.call(tos) !== '[object Array]') {
            tos = [tos];
        }
        
        for(var i = 0; i < tos.length; i++){
        	var webhookJobPayload = {};

        	webhookJobPayload.receiverEmailId = tos[i];
        	if(formPayload.toname && Object.prototype.toString.call(formPayload.toname) === '[object Array]' && formPayload.toname[i]) {
        		webhookJobPayload.receiverEmailName = formPayload.toname[i];
        	}

			for(var key in formPayload){
				webhookJobPayload[key] = formPayload[key];
				// if(formPayload.hasOwnProperty(key)) {
				// 	webhookJobPayload[key] = formPayload[key];
				// }
			}

			var content = formPayload['html'] || formPayload['text'] || "";
			webhookJobPayload['links'] = this.extractLinks(content);

			if(webhookJobPayload['html']) delete webhookJobPayload['html'];
			if(webhookJobPayload['text']) delete webhookJobPayload['text'];

			webhookJobPayload.stage = this.webhookEvents[0];
			webhookJobPayload.scheduleAt = this.getDateAfterSeconds(2);

			if(webhookJobPayload['to']) delete webhookJobPayload['to'];
			if(webhookJobPayload['toname']) delete webhookJobPayload['toname'];

			webhookJobPayloads.push(webhookJobPayload);
        }

		return webhookJobPayloads;
	},

	getDateAfterSeconds: function(numSeconds){
		return new Date(new Date().getTime() + numSeconds * 1000);
	},

	extractLinks: function(content){
	    var matchResult = content.match(URL_MATCH_REGEX);
	    if(matchResult === null) return [];
	    else return matchResult;
	}
};


module.exports = utils;
