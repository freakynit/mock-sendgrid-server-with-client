// constants
var WEBHOOK_JOB = "fire_webhook";
var SEND_REAL_MAIL_JOB = "send_real_mail";

var MAX_ALLOWED_RATE = 100;
var MOCK_ERROR_RATE = 0;
var ENABLE_WEBHOOKS = true;
var LOG_WEBHOOK_PAYLOAD_TO_CONSOLE = true;

var LOG_WEBHOOK_PAYLOAD_TO_FILE = true;
var WEBHOOK_PAYLOAD_FILE_PATH = "./webhookResponses.txt";

var ENABLE_SEND_REAL_MAIL = true;
var USE_TEST_RECEPIENT_FOR_REAL_MAIL = true;
var FAST_FORWARD_SCHEDULES = true;
var DEFAULT_PERIOD_BETWEEN_EACH_STAGE = 1;  // Seconds

// requires
var express = require('express');
var request = require('request');
var router = express.Router();
var multer  = require('multer');
var fs = require('fs');
var utils = require('./utils');
var upload = multer({dest: 'uploads/'});
var Agenda = require('agenda');
var mongoDb = require('mongodb');
var MongoClient = mongoDb.MongoClient;
var ObjectId = mongoDb.ObjectID;

utils.init();
utils.setErrorRate(MOCK_ERROR_RATE);

var agenda = null;

var overallStats = {
    "numRequests": 0,
    "maxRequestRate": MAX_ALLOWED_RATE,
    "maxErrorRate": MOCK_ERROR_RATE,
    "curRequestRate": 0,
    "successCount": 0,
    "failureCount": 0,
    "serverErrorCount": 0,
    "lastErrorMessage": ""
};

var staticConfig = {
    "webhooksUrl": "",
    "testRecepient": ""
};

// mongo
var url = 'mongodb://localhost:27017/sendgrid';
var db = null;
var stats = null;
var payloads = null;
var webhookResponses = null;

var cl = function(msg){
    console.log(msg);
};

MongoClient.connect(url, function(err, _db) 
{
    if(err) throw new Error(err);
    cl("Connected successfully to mongo server");

    // Mongo
    db = _db;
    stats = _db.collection('stats');
    payloads = _db.collection('payloads');
    webhookResponses = _db.collection('webhookResponses');

    // Agenda
    agenda = new Agenda({
        mongo: _db, 
        processEvery: '1 seconds',
        defaultConcurrency: 100,
        maxConcurrency: 200
    });
    agenda.on('ready', function(){
        agenda.start();
        defineJobs();
    });

    agenda.on('error', function(err){
        cl(err);
    });

    agenda.on('start', function(job) {
        console.log("Starting job: %s", job.attrs.name);
    });

    agenda.on('fail', function(err, job) {
        console.log("!!!!!!!!!!Job '" + job.attrs.name + "' failed with error: %s", err.message);
    });

    agenda.on('complete', function(job) {
        console.log("Completed job: %s", job.attrs.name);
    });
});

// helpers
var numRequests = 0;
var startTimeInSecs = new Date().getTime() / 1000;

var rateRefresher = setInterval(function(){
    numRequests = 0;
    startTimeInSecs = new Date().getTime() / 1000;
}, 10000);

var statsInserter = setInterval(function(){
    try {
        if(overallStats["_id"]) {
            stats.updateOne({
                "_id": new ObjectId(overallStats["_id"])
            }, {
                "$set": overallStats
            }, 
            function(err, result){});
        } else {
            stats.insertOne(overallStats, function(err, result){});
        }
    } catch(err){

    }
}, 5000);

var getCurrentRequestRate = function(){
    return numRequests/(new Date().getTime() / 1000 - startTimeInSecs);
};

var isRequestRateLimitOkay = function(){
    if(MAX_ALLOWED_RATE > 0) {
        return getCurrentRequestRate() < MAX_ALLOWED_RATE;
    } else {
        return true;
    }
};

var sendSuccess = function(res){
    res.json({success: true});
};

var sendError = function(res, errorMessage){
    res.json({success: false, error: errorMessage});
};

var defineJobs = function(){
    // agenda.define('dummy_job', {priority: "high", concurrency: 10}, function(job, done) {
    //     var jobData = job.attrs.data;
    //     var webhookJobPayload = jobData.webhookJobPayload;

    //     setTimeout(function(){
    //         console.log("running dummy_job");
    //         agenda.schedule(utils.getDateAfterSeconds(DEFAULT_PERIOD_BETWEEN_EACH_STAGE), 'dummy_job', {"webhookJobPayload": webhookJobPayload});
            
    //         done();
    //     }, 1000);
    // });

    agenda.define(SEND_REAL_MAIL_JOB, {concurrency: 2}, function(job, done) {
        var jobData = job.attrs.data;
        var formPayload = jobData.formPayload;

        if(formPayload.to) {
            var tos = formPayload.to;
            if(Object.prototype.toString.call(tos) !== '[object Array]') {
                tos = [tos];
            }

            var tonames = null;
            if(formPayload.toname) {
                if(Object.prototype.toString.call(tonames) !== '[object Array]') {
                    tonames = [tonames];
                }
            }

            var mailData = {
                to: tos,
                toname: tonames,
                subject: formPayload.subject,
                text: formPayload.text,
                html: formPayload.html
            };
            
            utils.sendMail(mailData, function(error, info){
                if(error){
                    cl("Send mail error: ", error);
                } else {
                    cl('Email sent. Response: ' + info.response);
                }

                done();
            });
        } else {
            setTimeout(function(){
                done();
            }, 100);
        }
    });

    agenda.define(WEBHOOK_JOB, {priority: "high", concurrency: 100}, function(job, done) {
        var jobData = job.attrs.data;
        var webhookJobPayload = jobData.webhookJobPayload;

        var responses = [];
        
        var webhooksResponsePayload = utils.getWebhookResponseDetails(webhookJobPayload, webhookJobPayload.receiverEmailId, webhookJobPayload.receiverEmailName);
        if(webhooksResponsePayload.stage == "end") {
            cl("---- END:END:END ----");
            
            setTimeout(function(){
                done();
            }, 100);
        } else {
            var dataForNextJobIteration = utils.clone(webhookJobPayload)
            dataForNextJobIteration.stage = webhooksResponsePayload.stage;
            
            if(webhooksResponsePayload.scheduleAt) {
                agenda.schedule(webhooksResponsePayload.scheduleAt, WEBHOOK_JOB, {"webhookJobPayload": dataForNextJobIteration});
            } else {
                agenda.schedule(utils.getDateAfterSeconds(DEFAULT_PERIOD_BETWEEN_EACH_STAGE), WEBHOOK_JOB, {"webhookJobPayload": dataForNextJobIteration});
            }
            
            if(webhooksResponsePayload.payload != null){
                responses.push(webhooksResponsePayload.payload);

                if(LOG_WEBHOOK_PAYLOAD_TO_CONSOLE == true) {
                    cl("\nResponses For Webhook: \n" + JSON.stringify(responses));
                }

                if(LOG_WEBHOOK_PAYLOAD_TO_FILE == true) {
                    fs.appendFileSync(WEBHOOK_PAYLOAD_FILE_PATH, JSON.stringify(responses), 'utf-8');
                }

                webhookResponses.insert(responses, function(err, result){
                    if(ENABLE_WEBHOOKS === true && staticConfig.webhooksUrl != null && staticConfig.webhooksUrl.length > 0) {
                        try {
                            request({
                                method: 'post',
                                url: staticConfig.webhooksUrl, 
                                body: responses,
                                json: true,
                                headers: {
                                    'Content-Type': 'application/json; charset=utf-8'
                                }
                            }, function(err, httpResponse, body) {
                                if(err) {
                                    cl("error: ", err);
                                } else {
                                    cl("webhook successfully posted: " + body);
                                }
                                
                                done();
                            });
                        } catch(err){
                            console.log("[webhook post error] ", err);
                            setTimeout(function(){
                                done();
                            }, 100);
                        }
                    } else {
                        setTimeout(function(){
                            done();
                        }, 100);        
                    }
                });
            }
        }
    });
};


// routes

// Configuration
router.post('/api/config/maxRequestRate', function(req, res){
    try {
        var maxRequestRate = parseInt(req.body['value'], 10);
        
        MAX_ALLOWED_RATE = maxRequestRate;
        
        overallStats.maxRequestRate = maxRequestRate;
        
        sendSuccess(res);
    } catch(err){
        sendError(res, err.toString());
    }
});

router.post('/api/config/maxErrorRate', function(req, res){
    try {
        var maxErrorRate = parseFloat(req.body['value']);

        utils.setErrorRate(maxErrorRate);        
        overallStats.maxErrorRate = maxErrorRate;
        
        sendSuccess(res);
    } catch(err){
        sendError(res, err.toString());
    }
});

router.post('/api/config/webhooksUrl', function(req, res){
    try {
        staticConfig.webhooksUrl = req.body['value'];
        sendSuccess(res);
    } catch(err){
        sendError(res, err.toString());
    }
});

router.post('/api/config/testRecepient', function(req, res){
    try {
        staticConfig.testRecepient = req.body['value'];
        sendSuccess(res);
    } catch(err){
        sendError(res, err.toString());
    }
});


// Main
router.get('/api/stats', function(req, res){
    res.json({"overallStats": overallStats, "staticConfig": staticConfig});
});

router.get('/stats', function(req, res){
    //res.json(overallStats);
    res.render('index/index', {title: 'SendGrid Mock Server Web UI', 'overallStats': overallStats, "staticConfig": staticConfig});
});

var insertPayloadWithResponse = function(formPayload, response, cb){
    formPayload.response = response;
    payloads.insertOne(formPayload, cb);
};

var constructResponseObject = function(statusCode, responseJson){
    return {
        "statusCode": statusCode,
        "responseJson": responseJson
    };
};

var manageSendingRealMail = function(formPayload){
    if(ENABLE_SEND_REAL_MAIL == true) {
        var dataForSendMail = formPayload;

        if(USE_TEST_RECEPIENT_FOR_REAL_MAIL == true && staticConfig.testRecepient != null && staticConfig.testRecepient.length > 0) {
            dataForSendMail = utils.clone(formPayload);
            dataForSendMail.to = [staticConfig.testRecepient];
        } else {
            dataForSendMail = null;
        }

        if(dataForSendMail != null) {
            agenda.schedule(utils.getDateAfterSeconds(DEFAULT_PERIOD_BETWEEN_EACH_STAGE), SEND_REAL_MAIL_JOB, {"formPayload": formPayload});
        }
    }
};

router.post('/', upload.array('files'), function(req, res){
    numRequests++;
    overallStats.numRequests++;
    var response = {};

    var formPayload = req.body;
    if(formPayload['x-smtpapi'] && (typeof formPayload['x-smtpapi']) !== "object") {
        formPayload['x-smtpapi'] = JSON.parse(formPayload['x-smtpapi']);
    }

    if(overallStats.maxErrorRate > 0) {
        formPayload = utils.mockErrorInFormPayload(formPayload);
    }

    try {
        var curRequestRate = getCurrentRequestRate();
        overallStats.curRequestRate = curRequestRate;
        //cl("request rate: " + curRequestRate);

        if(isRequestRateLimitOkay() === false) {
            //cl("request rate exceeded");
            responseJson = {
                "errors": ["Request rate exceeded"],
                "message": "error"
            };
            response = constructResponseObject(400, responseJson);
            overallStats.failureCount++;
            overallStats.lastErrorMessage = responseJson.errors;
        } else {
            var responseJson = utils.getResponseJson(formPayload);
            if(responseJson.errors) {
                overallStats.failureCount++;
                overallStats.lastErrorMessage = responseJson.errors;
                response = constructResponseObject(400, responseJson);
            } else {
                var throw500 = Math.floor((Math.random() * 100) + 1);
                if(throw500 < 2) {
                    var errorMessage = "Dummy 500 at server";
                    cl("throwing '" + errorMessage + "' error message");
                    throw new Error(errorMessage);
                }
                
                overallStats.successCount++;
                response = constructResponseObject(200, responseJson);

                try {
                    var webhookJobPayloads = utils.constructWebhookJobPayload(formPayload);
                    for(var i = 0; i < webhookJobPayloads.length; i++){
                        var curJobPayload = webhookJobPayloads[i];

                        agenda.schedule(curJobPayload.scheduleAt, WEBHOOK_JOB, {"webhookJobPayload": curJobPayload});
                    }

                    manageSendingRealMail(formPayload);
                } catch(err){
                    cl("[error: agenda]", err.toString());
                }
            }
        }
    } catch(err){
        responseJson = {
            "errors": ["Internal server error: " + err.toString()],
            "message": "error"
        };
        overallStats.serverErrorCount++;
        overallStats.lastErrorMessage = responseJson.errors;
        response = constructResponseObject(500, responseJson);
    }

    try {
        insertPayloadWithResponse(formPayload, response, function(){
            res.status(response.statusCode).send(JSON.stringify(response.responseJson));
        });
    } catch(err){
        responseJson = {
            "errors": ["Internal server error: " + err.toString()],
            "message": "error"
        };
        overallStats.serverErrorCount++;
        overallStats.lastErrorMessage = responseJson.errors;
        
        response = constructResponseObject(500, responseJson);
        res.status(response.statusCode).send(JSON.stringify(response.responseJson));
    }
});

module.exports.router = router;


function graceful() {
    cl("gracefully shutting down");
    
    db.close();
    agenda.stop(function() {
        cl("stopped agenda");
        process.exit(0);
    });
}

process.on('SIGTERM', graceful);
process.on('SIGINT' , graceful);
process.on('SIGHUP' , graceful);
process.on('SIGQUIT' , graceful);
process.on('SIGABRT' , graceful);
