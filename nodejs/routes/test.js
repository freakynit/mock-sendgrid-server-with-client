var randomip = require('random-ip');
var nodemailer = require('nodemailer');
var fs = require('fs');

var cl = function(msg){
    console.log(msg);
};

var utils = {
	maxErrorRate: 0.2,
	transporter: null,
	deferredRetryAfterSeconds: 5,
	userAgents: [
		"Mozilla/5.0 (Linux; Android 5.1.1; SM-G928X Build/LMY47X) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36",
		"Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 950) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Mobile Safari/537.36 Edge/13.10586",
		"Mozilla/5.0 (Linux; Android 6.0.1; Nexus 6P Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.83 Mobile Safari/537.36",
		"Mozilla/5.0 (Linux; Android 5.0.2; SAMSUNG SM-T550 Build/LRX22G) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/3.3 Chrome/38.0.2125.102 Safari/537.36",
		"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/601.3.9 (KHTML, like Gecko) Version/9.0.2 Safari/601.3.9",
		"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36",
		"Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1",
		"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)",
		"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
		"Mozilla/5.0 (Macintosh; U; PPC Mac OS X; fi-fi) AppleWebKit/420+ (KHTML, like Gecko) Safari/419.3",
		"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1b3) Gecko/20090305 Firefox/3.1b3 GTB5",
		"Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/534.4 (KHTML, like Gecko) Chrome/6.0.481.0 Safari/534.4"
	],
	bounceReasons: [
		"500 unknown recipient",
		"User not found",
		"Some other reason"
	],
	dropReasons: [
		"Bounced Address",
		"Address not found",
		"Some other reason"
	],
	emailCategories: ["cat facts", "dog facts", "human facts", "alien facts"],
	webhookEventDetails: {
		/*"start": {
			"getProbability": function(){
				return 0;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				
			},
			"eventType": "delivery"
		},
		
		*/"processed": {
			"getProbability": function(){
				return 1;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				"hi": "there"	//TODO: remove this
			},
			"eventType": "delivery"
		},

		"dropped": {
			"getProbability": function(){
				return 0.01;
			},
			"marksEnd": true,
			"retry": {
				"capable": false
			},
			"data": {
				"reason": function(){
					return this.dropReasons[utils.getRandomNumber(0, this.dropReasons.length)];
				},
	    		"status": "5.0.0"
			},
			"eventType": "delivery"
		},
		
		"deferred": {
			"getProbability": function(){
				return 0.01;
			},
			"marksEnd": false,
			"retry": {
				"capable": true,
				"getProbability": function(){
					return 0.1;
				}
			},
			"data": {
				"response": "400 try again later",
				"attempt": function(){
					return utils.getRandomNumber(0, 5).toString();
				}	
			},
			"eventType": "delivery"
		},

		"bounce": {
			"getProbability": function(){
				return 0.02;
			},
			"marksEnd": true,
			"retry": {
				"capable": false
			},
			"data": {
				"reason": function(){
					return this.bounceReasons[utils.getRandomNumber(0, this.bounceReasons.length)];
				},
				"status": "5.0.0"	
			},
			"eventType": "delivery"
		},
		
		"delivered": {
			"getProbability": function(){
				return 1;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				"response": "250 OK"	
			},
			"eventType": "delivery"
		},

		"open": {
			"getProbability": function(){
				return 0.25;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				"useragent": function(){
					return this.userAgents[utils.getRandomNumber(0, this.userAgents.length)];
				},
				"ip": function(){
					return randomip('0.0.0.0');
				}	
			},
			"eventType": "delivery"
		},

		"click": {
			"getProbability": function(){
				return 0.1;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				"useragent": function(){
					return utils.userAgents[utils.getRandomNumber(0, utils.userAgents.length)];
				},
				"ip": function(){
					return randomip('0.0.0.0');
				},
				"url": function(links){
					if(links && links.length > 0) {
						return utils.links[utils.getRandomNumber(0, utils.links.length)];
					} else {
						return "http://sendgrid.com/";
					}
				}	
			},
			"eventType": "engagement"
		},

		"spamreport": {
			"getProbability": function(){
				return 0.05;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				
			},
			"eventType": "engagement"
		},

		"unsubscribe": {
			"getProbability": function(){
				return 0.025;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				
			},
			"eventType": "engagement"
		},

		"group_unsubscribe": {
			"getProbability": function(){
				return 0.05;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				"useragent": function(){
					return this.userAgents[utils.getRandomNumber(0, this.userAgents.length)];
				},
				"ip": function(){
					return randomip('0.0.0.0');
				},
				"url": function(links){
					if(links && links.length > 0) {
						return this.links[utils.getRandomNumber(0, this.links.length)];
					} else {
						return "http://sendgrid.com/";
					}
				},
				"asm_group_id": function(){
					return utils.getRandomNumber(0, 10);
				}	
			},
			"eventType": "engagement"
		},

		"group_resubscribe": {
			"getProbability": function(){
				return 0.001;
			},
			"marksEnd": false,
			"retry": {
				"capable": false
			},
			"data": {
				"useragent": function(){
					return this.userAgents[utils.getRandomNumber(0, this.userAgents.length)];
				},
				"ip": function(){
					return randomip('0.0.0.0');
				},
				"url": function(links){
					if(links && links.length > 0) {
						return this.links[utils.getRandomNumber(0, this.links.length)];
					} else {
						return "http://sendgrid.com/";
					}
				},
				"asm_group_id": function(){
					return utils.getRandomNumber(0, 10);
				}	
			},
			"eventType": "engagement"
		}
	},

	webhookEvents: [],

	init: function(){
		this.transporter = nodemailer.createTransport('smtps://fa80418%40gmail.com:admin123A@@smtp.gmail.com');

		for(key in this.webhookEventDetails){
			if(this.webhookEventDetails.hasOwnProperty(key)) {
				this.webhookEvents.push(key);
			}
		}
	},

	getResponseJson: function(formPayload){
		var errors = [];

		if(formPayload['from'] === undefined || formPayload['from'].length < 1) {
			errors.push('Empty from email address (required)');
		}

		if(formPayload['subject'] === undefined || formPayload['subject'].length < 1) {
			errors.push('Missing subject');
		}

		if((formPayload['text'] === undefined || formPayload['text'].length < 1) 
			&& (formPayload['html'] === undefined || formPayload['html'].length < 1)) {
			errors.push('Missing email body');
		}

		if(formPayload['toname'] !== undefined && formPayload['toname'].length != formPayload['to'].length) {
			errors.push('Parameters to and toname should be the same length');
		}

		if(errors.length > 0) {
			return {
				"errors": errors,
				"message": "error"
			};
		} else {
			return {
				"message": "success"
			};
		}
	},

	setErrorRate: function(errRate){
		this.maxErrorRate = errRate;
	},

	getRandomProbability: function(){
		return Math.random();
	},

	getRandomNumber: function(min, max){
		return Math.floor(Math.random() * max) + min;
	},

	mockErrorInFormPayload: function(formPayload){
		if(this.maxErrorRate > 0) {
			if(this.maxErrorRate >= this.getRandomProbability()) {
				return this.introduceErrorInFormPayload(formPayload);
			} else {
				return formPayload;
			}
		} else {
			return formPayload;
		}
	},

	randomString: function(length){
		return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
	},

	introduceErrorInFormPayload: function(formPayload){
		var curErrorProbabilityThreshold = this.getRandomProbability();

		if(curErrorProbabilityThreshold < 0.3) {
			delete formPayload["from"];
		} else if(curErrorProbabilityThreshold >= 0.3 && curErrorProbabilityThreshold < 0.6) {
			delete formPayload["subject"];
		} else if(curErrorProbabilityThreshold >= 0.6 && curErrorProbabilityThreshold < 0.9) {
			delete formPayload["text"];
			delete formPayload["html"];
		} else {
			if(formPayload["toname"] && formPayload["toname"].length > 0) {
				formPayload["toname"].splice(0, 1);
			}
		}

		return formPayload;
	},

	getRandomIp: function(){
		return randomip('0.0.0.0');
	},

	getWebhookResponseDetails: function(webhookJobPayload, emailId){
		var webhookResponseDetails = {
			"payload": null,
			"stage": "end"
		};

		if(webhookJobPayload.stage == null || webhookJobPayload.stage === undefined || webhookJobPayload.stage.length < 1) {
			cl("Undefined webhookJobPayload.stage");
			return webhookResponseDetails;
		}
	
		var curStageEventName = webhookJobPayload.stage;
		var curStageEventDetails = this.webhookEventDetails[curStageEventName];

		var nextEventIndex = this.webhookEvents.indexOf(curStageEventName) + 1;
		if(nextEventIndex < this.webhookEvents.length) {
			var nextStageEventName = this.webhookEvents[nextEventIndex];

			if(curStageEventDetails.getProbability() >= this.getRandomProbability()) {
				webhookResponseDetails["payload"] = this.generateRandomDataForWebhookEvent(webhookJobPayload, emailId, curStageEventName);

				if(curStageEventDetails.marksEnd == false) {
					if(curStageEventDetails.retry.capable == true
						&& (curStageEventDetails.retry.getProbability() >= this.getRandomProbability())) {
						webhookResponseDetails["stage"] = curStageEventName;
					} else {
						webhookResponseDetails.stage = nextStageEventName;
					}
				}
			} else {
				webhookResponseDetails["payload"] = null;
				webhookResponseDetails.stage = nextStageEventName;
			}
		}

		return webhookResponseDetails;
	},

	generateRandomDataForWebhookEvent: function(webhookJobPayload, emailId, eventName){
		var responseData = {
			"email": emailId,
			"timestamp": Math.floor(new Date().getTime() / 1000),
			"event": eventName,
			"category": this.emailCategories[this.getRandomNumber(0, this.emailCategories.length)],
			"sg_event_id": this.randomString(20),
			"sg_message_id": this.randomString(50)
		};

		// Add SMTP data
		var xSmtpapiDetails = webhookJobPayload['x-smtpapi'];
		for(var key in xSmtpapiDetails.unique_args){
			if(xSmtpapiDetails.unique_args.hasOwnProperty(key)) {
				responseData[key] = xSmtpapiDetails.unique_args[key];
			}
		}

		var eventExtraData = this.webhookEventDetails[eventName].data;
		for(var key in eventExtraData){
			if(eventExtraData.hasOwnProperty(key)) {
				var val = null;
				if(Object.prototype.toString.call(eventExtraData[key], webhookJobPayload.links) == '[object Function]') {
					val = eventExtraData[key].call(webhookJobPayload.links);
				} else {
					val = eventExtraData[key];
				}

				responseData[key] = val;
			}
		}

		return responseData;
	},

	shoudRetry: function(webhooksresponsePayload){
		if(webhooksresponsePayload.event == "deferred") {
			var curdate = new Date();
			var nextTryAt = new Date(curdate.getTime() + this.deferredRetryAfterSeconds);
			return {
				"retry": true,
				"nextTryAt": nextTryAt
			};
		} else {
			return {
				"retry": false
			};
		}
	},

	sendMail: function(formPayload, cb){
		var mailOptions = {
		    from: '"fa80418" <fa80418@gmail.com>', // sender address
		    to: formPayload.to.join(","),
		    subject: formPayload.subject,
		    text: formPayload.text,
		    html: formPayload.html
		};

		this.transporter.sendMail(mailOptions, cb);
	},

	clone: function(obj){
		return JSON.parse(JSON.stringify(obj));
	},

	constructWebhookJobPayload: function(formPayload){
		var webhookJobPayload = {};
		for(var key in formPayload){
			if(formPayload.hasOwnProperty(key)) {
				webhookJobPayload[key] = formPayload[key];
			}
		}

		var content = formPayload['html'] || formPayload['text'] || "";
		webhookJobPayload['links'] = this.extractLinks(content);

		if(webhookJobPayload['html']) delete webhookJobPayload['html'];
		if(webhookJobPayload['text']) delete webhookJobPayload['text'];

		webhookJobPayload.stage = this.webhookEvents[0];

		return webhookJobPayload;
	},

	extractLinks: function(content){
	    var matchResult = content.match(URL_MATCH_REGEX);
	    if(matchResult === null) return [];
	    else return matchResult;
	}
};


// var i = 0;
// var itvl = setInterval(function(){
// 	var responseData = {};
// 	var obj = utils.webhookEventDetailsGenerators.click;
// 	for(var key in obj){
// 		if(obj.hasOwnProperty(key)) {
// 			var val = null;
// 			if(Object.prototype.toString.call(obj[key]) == '[object Function]') {
// 				val = obj[key].call(undefined);
// 			} else {
// 				val = obj[key];
// 			}

// 			responseData[key] = val;
// 		}
// 	}

// 	console.log("\n" + JSON.stringify(responseData));
	
// 	//console.log(Math.floor(new Date().getTime() / 1000));
// 	if(i++ > 100) {
// 		clearInterval(itvl);
// 	}
// }, 10);

function test1(){
	var extractLinks = function(formPayload){
	    var content = formPayload['html'] || formPayload['text'] || "";

	    var matchResult = content.match(URL_MATCH_REGEX);
	    if(matchResult === null) return [];
	    else return matchResult;
	};

	var input = "This is a link to image http://example.com/logo.png http://google.com";
	console.log("output = " + extractLinks({
		text: "",
		html: input
	}));	
}

function test2(){
	var nodemailer = require('nodemailer');
	var smtpConfig = {
	    host: 'smtp.gmail.com',
	    port: 465,
	    secure: false, // use SSL
	    auth: {
	        user: 'fa80418@gmail.com',
	        pass: 'admin123A@'
	    }
	};

	var transporter = nodemailer.createTransport('smtps://fa80418%40gmail.com:admin123A@@smtp.gmail.com');

	// setup e-mail data with unicode symbols
	var mailOptions = {
	    from: '"fa80418" <fa80418@gmail.com>', // sender address
	    to: 'nitin@webklipper.com', // list of receivers
	    subject: 'Hello ✔', // Subject line
	    text: 'Hello world 🐴', // plaintext body
	    html: '<b>Hello world 🐴</b>' // html body
	};

	// send mail with defined transport object
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        return console.log(error);
	    }
	    console.log('Message sent: ' + info.response);
	});
}

function test3(){
	var formPayload = {"to":["nitin@webklipper.com","nitinbansal85@gmail.com","nitinbansal85.2@gmail.com"],"toname":["nitin-webklipper","nitinbansal85","nitinbansal85.2"],"cc":["nitinbansal85.3@gmail.com"],"bcc":["nitinbansal85.4@gmail.com"],"from":"niitn@webklipper.com","replyto":"nitin@webklipper.com","subject":"Hello World","html":"This is a link to image http://example.com/logo.png and this is a link pointing to Google http://google.com"};
	utils.sendMail(formPayload, function(error, info){
        if(error){
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
    });
}

function test4(){
	var extractLinks = function(formPayload){
	    var content = formPayload['html'] || formPayload['text'] || "";

	    var matchResult = content.match(URL_MATCH_REGEX);
	    if(matchResult === null) return [];
	    else return matchResult;
	};

	var content = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width"><style>#outlook a{padding:0;}body{width:100%!important;min-width:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;Margin:0;padding:0;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;}.ExternalClass{width:100%;}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}#backgroundTable{margin:0;Margin:0;padding:0;width:100%!important;line-height:100%!important;}img{outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:auto;max-width:100%;clear:both;display:block;}center{width:100%;min-width:580px;}a img{border:none;}p{margin:0 0 0 10px;Margin:0 0 0 10px;}table{border-spacing:0;border-collapse:collapse;}td{word-wrap:break-word;-webkit-hyphens:auto;-moz-hyphens:auto;hyphens:auto;border-collapse:collapse!important;}table,tr,td{padding:0;vertical-align:top;text-align:left;}html{min-height:100%;background:#f3f3f3;}table.body{background:#f3f3f3;height:100%;width:100%;}table.container{background:#fefefe;width:580px;margin:0 auto;Margin:0 auto;text-align:inherit;}table.row{padding:0;width:100%;position:relative;}table.container table.row{display:table;}td.columns,td.column,th.columns,th.column{margin:0 auto;Margin:0 auto;padding-left:16px;padding-bottom:16px;}td.columns.last,td.column.last,th.columns.last,th.column.last{padding-right:16px;}td.columns table,td.column table,th.columns table,th.column table{width:100%;}td.large-1,th.large-1{width:32.33333px;padding-left:8px;padding-right:8px;}td.large-1.first,th.large-1.first{padding-left:16px;}td.large-1.last,th.large-1.last{padding-right:16px;}.collapse>tbody>tr>td.large-1,.collapse>tbody>tr>th.large-1{padding-right:0;padding-left:0;width:48.33333px;}.collapse td.large-1.first,.collapse th.large-1.first,.collapse td.large-1.last,.collapse th.large-1.last{width:56.33333px;}td.large-2,th.large-2{width:80.66667px;padding-left:8px;padding-right:8px;}td.large-2.first,th.large-2.first{padding-left:16px;}td.large-2.last,th.large-2.last{padding-right:16px;}.collapse>tbody>tr>td.large-2,.collapse>tbody>tr>th.large-2{padding-right:0;padding-left:0;width:96.66667px;}.collapse td.large-2.first,.collapse th.large-2.first,.collapse td.large-2.last,.collapse th.large-2.last{width:104.66667px;}td.large-3,th.large-3{width:129px;padding-left:8px;padding-right:8px;}td.large-3.first,th.large-3.first{padding-left:16px;}td.large-3.last,th.large-3.last{padding-right:16px;}.collapse>tbody>tr>td.large-3,.collapse>tbody>tr>th.large-3{padding-right:0;padding-left:0;width:145px;}.collapse td.large-3.first,.collapse th.large-3.first,.collapse td.large-3.last,.collapse th.large-3.last{width:153px;}td.large-4,th.large-4{width:177.33333px;padding-left:8px;padding-right:8px;}td.large-4.first,th.large-4.first{padding-left:16px;}td.large-4.last,th.large-4.last{padding-right:16px;}.collapse>tbody>tr>td.large-4,.collapse>tbody>tr>th.large-4{padding-right:0;padding-left:0;width:193.33333px;}.collapse td.large-4.first,.collapse th.large-4.first,.collapse td.large-4.last,.collapse th.large-4.last{width:201.33333px;}td.large-5,th.large-5{width:225.66667px;padding-left:8px;padding-right:8px;}td.large-5.first,th.large-5.first{padding-left:16px;}td.large-5.last,th.large-5.last{padding-right:16px;}.collapse>tbody>tr>td.large-5,.collapse>tbody>tr>th.large-5{padding-right:0;padding-left:0;width:241.66667px;}.collapse td.large-5.first,.collapse th.large-5.first,.collapse td.large-5.last,.collapse th.large-5.last{width:249.66667px;}td.large-6,th.large-6{width:274px;padding-left:8px;padding-right:8px;}td.large-6.first,th.large-6.first{padding-left:16px;}td.large-6.last,th.large-6.last{padding-right:16px;}.collapse>tbody>tr>td.large-6,.collapse>tbody>tr>th.large-6{padding-right:0;padding-left:0;width:290px;}.collapse td.large-6.first,.collapse th.large-6.first,.collapse td.large-6.last,.collapse th.large-6.last{width:298px;}td.large-7,th.large-7{width:322.33333px;padding-left:8px;padding-right:8px;}td.large-7.first,th.large-7.first{padding-left:16px;}td.large-7.last,th.large-7.last{padding-right:16px;}.collapse>tbody>tr>td.large-7,.collapse>tbody>tr>th.large-7{padding-right:0;padding-left:0;width:338.33333px;}.collapse td.large-7.first,.collapse th.large-7.first,.collapse td.large-7.last,.collapse th.large-7.last{width:346.33333px;}td.large-8,th.large-8{width:370.66667px;padding-left:8px;padding-right:8px;}td.large-8.first,th.large-8.first{padding-left:16px;}td.large-8.last,th.large-8.last{padding-right:16px;}.collapse>tbody>tr>td.large-8,.collapse>tbody>tr>th.large-8{padding-right:0;padding-left:0;width:386.66667px;}.collapse td.large-8.first,.collapse th.large-8.first,.collapse td.large-8.last,.collapse th.large-8.last{width:394.66667px;}td.large-9,th.large-9{width:419px;padding-left:8px;padding-right:8px;}td.large-9.first,th.large-9.first{padding-left:16px;}td.large-9.last,th.large-9.last{padding-right:16px;}.collapse>tbody>tr>td.large-9,.collapse>tbody>tr>th.large-9{padding-right:0;padding-left:0;width:435px;}.collapse td.large-9.first,.collapse th.large-9.first,.collapse td.large-9.last,.collapse th.large-9.last{width:443px;}td.large-10,th.large-10{width:467.33333px;padding-left:8px;padding-right:8px;}td.large-10.first,th.large-10.first{padding-left:16px;}td.large-10.last,th.large-10.last{padding-right:16px;}.collapse>tbody>tr>td.large-10,.collapse>tbody>tr>th.large-10{padding-right:0;padding-left:0;width:483.33333px;}.collapse td.large-10.first,.collapse th.large-10.first,.collapse td.large-10.last,.collapse th.large-10.last{width:491.33333px;}td.large-11,th.large-11{width:515.66667px;padding-left:8px;padding-right:8px;}td.large-11.first,th.large-11.first{padding-left:16px;}td.large-11.last,th.large-11.last{padding-right:16px;}.collapse>tbody>tr>td.large-11,.collapse>tbody>tr>th.large-11{padding-right:0;padding-left:0;width:531.66667px;}.collapse td.large-11.first,.collapse th.large-11.first,.collapse td.large-11.last,.collapse th.large-11.last{width:539.66667px;}td.large-12,th.large-12{width:564px;padding-left:8px;padding-right:8px;}td.large-12.first,th.large-12.first{padding-left:16px;}td.large-12.last,th.large-12.last{padding-right:16px;}.collapse>tbody>tr>td.large-12,.collapse>tbody>tr>th.large-12{padding-right:0;padding-left:0;width:580px;}.collapse td.large-12.first,.collapse th.large-12.first,.collapse td.large-12.last,.collapse th.large-12.last{width:588px;}td.large-1 center,th.large-1 center{min-width:0.33333px;}td.large-2 center,th.large-2 center{min-width:48.66667px;}td.large-3 center,th.large-3 center{min-width:97px;}td.large-4 center,th.large-4 center{min-width:145.33333px;}td.large-5 center,th.large-5 center{min-width:193.66667px;}td.large-6 center,th.large-6 center{min-width:242px;}td.large-7 center,th.large-7 center{min-width:290.33333px;}td.large-8 center,th.large-8 center{min-width:338.66667px;}td.large-9 center,th.large-9 center{min-width:387px;}td.large-10 center,th.large-10 center{min-width:435.33333px;}td.large-11 center,th.large-11 center{min-width:483.66667px;}td.large-12 center,th.large-12 center{min-width:532px;}.body .columns td.large-1,.body .column td.large-1,.body .columns th.large-1,.body .column th.large-1{width:8.33333%;}.body .columns td.large-2,.body .column td.large-2,.body .columns th.large-2,.body .column th.large-2{width:16.66667%;}.body .columns td.large-3,.body .column td.large-3,.body .columns th.large-3,.body .column th.large-3{width:25%;}.body .columns td.large-4,.body .column td.large-4,.body .columns th.large-4,.body .column th.large-4{width:33.33333%;}.body .columns td.large-5,.body .column td.large-5,.body .columns th.large-5,.body .column th.large-5{width:41.66667%;}.body .columns td.large-6,.body .column td.large-6,.body .columns th.large-6,.body .column th.large-6{width:50%;}.body .columns td.large-7,.body .column td.large-7,.body .columns th.large-7,.body .column th.large-7{width:58.33333%;}.body .columns td.large-8,.body .column td.large-8,.body .columns th.large-8,.body .column th.large-8{width:66.66667%;}.body .columns td.large-9,.body .column td.large-9,.body .columns th.large-9,.body .column th.large-9{width:75%;}.body .columns td.large-10,.body .column td.large-10,.body .columns th.large-10,.body .column th.large-10{width:83.33333%;}.body .columns td.large-11,.body .column td.large-11,.body .columns th.large-11,.body .column th.large-11{width:91.66667%;}.body .columns td.large-12,.body .column td.large-12,.body .columns th.large-12,.body .column th.large-12{width:100%;}td.large-offset-1,td.large-offset-1.first,td.large-offset-1.last,th.large-offset-1,th.large-offset-1.first,th.large-offset-1.last{padding-left:64.33333px;}td.large-offset-2,td.large-offset-2.first,td.large-offset-2.last,th.large-offset-2,th.large-offset-2.first,th.large-offset-2.last{padding-left:112.66667px;}td.large-offset-3,td.large-offset-3.first,td.large-offset-3.last,th.large-offset-3,th.large-offset-3.first,th.large-offset-3.last{padding-left:161px;}td.large-offset-4,td.large-offset-4.first,td.large-offset-4.last,th.large-offset-4,th.large-offset-4.first,th.large-offset-4.last{padding-left:209.33333px;}td.large-offset-5,td.large-offset-5.first,td.large-offset-5.last,th.large-offset-5,th.large-offset-5.first,th.large-offset-5.last{padding-left:257.66667px;}td.large-offset-6,td.large-offset-6.first,td.large-offset-6.last,th.large-offset-6,th.large-offset-6.first,th.large-offset-6.last{padding-left:306px;}td.large-offset-7,td.large-offset-7.first,td.large-offset-7.last,th.large-offset-7,th.large-offset-7.first,th.large-offset-7.last{padding-left:354.33333px;}td.large-offset-8,td.large-offset-8.first,td.large-offset-8.last,th.large-offset-8,th.large-offset-8.first,th.large-offset-8.last{padding-left:402.66667px;}td.large-offset-9,td.large-offset-9.first,td.large-offset-9.last,th.large-offset-9,th.large-offset-9.first,th.large-offset-9.last{padding-left:451px;}td.large-offset-10,td.large-offset-10.first,td.large-offset-10.last,th.large-offset-10,th.large-offset-10.first,th.large-offset-10.last{padding-left:499.33333px;}td.large-offset-11,td.large-offset-11.first,td.large-offset-11.last,th.large-offset-11,th.large-offset-11.first,th.large-offset-11.last{padding-left:547.66667px;}td.expander,th.expander{visibility:hidden;width:0;padding:0!important;}.block-grid{width:100%;max-width:580px;}.block-grid td{display:inline-block;padding:8px;}.up-2 td{width:274px!important;}.up-3 td{width:177px!important;}.up-4 td{width:129px!important;}.up-5 td{width:100px!important;}.up-6 td{width:80px!important;}.up-7 td{width:66px!important;}.up-8 td{width:56px!important;}table.text-center,td.text-center,h1.text-center,h2.text-center,h3.text-center,h4.text-center,h5.text-center,h6.text-center,p.text-center,span.text-center{text-align:center;}h1.text-left,h2.text-left,h3.text-left,h4.text-left,h5.text-left,h6.text-left,p.text-left,span.text-left{text-align:left;}h1.text-right,h2.text-right,h3.text-right,h4.text-right,h5.text-right,h6.text-right,p.text-right,span.text-right{text-align:right;}span.text-center{display:block;width:100%;text-align:center;}@media only screen and (max-width: 596px){.small-float-center{margin:0 auto!important;float:none!important;text-align:center!important;}.small-text-center{text-align:center!important;}.small-text-left{text-align:left!important;}.small-text-right{text-align:right!important;}}img.float-left{float:left;text-align:left;}img.float-right{float:right;text-align:right;}img.float-center,img.text-center{margin:0 auto;Margin:0 auto;float:none;text-align:center;}table.float-center,td.float-center,th.float-center{margin:0 auto;Margin:0 auto;float:none;text-align:center;}table.body table.container .hide-for-large{display:none;width:0;mso-hide:all;overflow:hidden;max-height:0px;font-size:0;width:0px;line-height:0;}@media only screen and (max-width: 596px){table.body table.container .hide-for-large{display:block!important;width:auto!important;overflow:visible!important;}}table.body table.container .hide-for-large *{mso-hide:all;}@media only screen and (max-width: 596px){table.body table.container .row.hide-for-large,table.body table.container .row.hide-for-large{display:table!important;width:100%!important;}}@media only screen and (max-width: 596px){table.body table.container .show-for-large{display:none!important;width:0;mso-hide:all;overflow:hidden;}}body,table.body,h1,h2,h3,h4,h5,h6,p,td,th,a{color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-weight:normal;padding:0;margin:0;Margin:0;text-align:left;line-height:1.3;}h1,h2,h3,h4,h5,h6{color:inherit;word-wrap:normal;font-family:Helvetica,Arial,sans-serif;font-weight:normal;margin-bottom:10px;Margin-bottom:10px;}h1{font-size:34px;}h2{font-size:30px;}h3{font-size:28px;}h4{font-size:24px;}h5{font-size:20px;}h6{font-size:18px;}body,table.body,p,td,th{font-size:16px;line-height:19px;}p{margin-bottom:10px;Margin-bottom:10px;}p.lead{font-size:20px;line-height:1.6;}p.subheader{margin-top:4px;margin-bottom:8px;Margin-top:4px;Margin-bottom:8px;font-weight:normal;line-height:1.4;color:#8a8a8a;}small{font-size:80%;color:#cacaca;}a{color:#2199e8;text-decoration:none;}a:hover{color:#147dc2;}a:active{color:#147dc2;}a:visited{color:#2199e8;}h1 a,h1 a:visited,h2 a,h2 a:visited,h3 a,h3 a:visited,h4 a,h4 a:visited,h5 a,h5 a:visited,h6 a,h6 a:visited{color:#2199e8;}pre{background:#f3f3f3;margin:30px 0;Margin:30px 0;}pre code{color:#cacaca;}pre code span.callout{color:#8a8a8a;font-weight:bold;}pre code span.callout-strong{color:#ff6908;font-weight:bold;}hr{max-width:580px;height:0;border-right:0;border-top:0;border-bottom:1px solid #cacaca;border-left:0;margin:20px auto;Margin:20px auto;clear:both;}.stat{font-size:40px;line-height:1;}p+.stat{margin-top:-16px;Margin-top:-16px;}table.button{width:auto!important;margin:0 0 16px 0;Margin:0 0 16px 0;}table.button table td{width:auto!important;text-align:left;color:#fefefe;background:#2199e8;border:2px solid #2199e8;}table.button table td.radius{border-radius:3px;}table.button table td.rounded{border-radius:500px;}table.button table td a{font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:bold;color:#fefefe;text-decoration:none;display:inline-block;padding:8px 16px 8px 16px;border:0px solid #2199e8;border-radius:3px;}table.button:hover table tr td a,table.button:active table tr td a,table.button table tr td a:visited,table.button.tiny:hover table tr td a,table.button.tiny:active table tr td a,table.button.tiny table tr td a:visited,table.button.small:hover table tr td a,table.button.small:active table tr td a,table.button.small table tr td a:visited,table.button.large:hover table tr td a,table.button.large:active table tr td a,table.button.large table tr td a:visited{color:#fefefe;}table.button.tiny table td,table.button.tiny table a{padding:4px 8px 4px 8px;}table.button.tiny table a{font-size:10px;font-weight:normal;}table.button.small table td,table.button.small table a{padding:5px 10px 5px 10px;font-size:12px;}table.button.large table a{padding:10px 20px 10px 20px;font-size:20px;}table.expand,table.expanded{width:100%!important;}table.expand table,table.expanded table{width:100%;}table.expand table a,table.expanded table a{width:calc(100% - 20px);text-align:center;}table.expand center,table.expanded center{min-width:0;}table.button:hover table td,table.button:visited table td,table.button:active table td{background:#147dc2;color:#fefefe;}table.button:hover table a,table.button:visited table a,table.button:active table a{border:0px solid #147dc2;}table.button.secondary table td{background:#777777;color:#fefefe;border:2px solid #777777;}table.button.secondary table a{color:#fefefe;border:0px solid #777777;}table.button.secondary:hover table td{background:#919191;color:#fefefe;}table.button.secondary:hover table a{border:0px solid #919191;}table.button.secondary:hover table td a{color:#fefefe;}table.button.secondary:active table td a{color:#fefefe;}table.button.secondary table td a:visited{color:#fefefe;}table.button.success table td{background:#3adb76;border:2px solid #3adb76;}table.button.success table a{border:0px solid #3adb76;}table.button.success:hover table td{background:#23bf5d;}table.button.success:hover table a{border:0px solid #23bf5d;}table.button.alert table td{background:#ec5840;border:2px solid #ec5840;}table.button.alert table a{border:0px solid #ec5840;}table.button.alert:hover table td{background:#e23317;}table.button.alert:hover table a{border:0px solid #e23317;}table.callout{margin-bottom:16px;Margin-bottom:16px;}th.callout-inner{width:100%;border:1px solid #cbcbcb;padding:10px;background:#fefefe;}th.callout-inner.primary{background:#def0fc;border:1px solid #444444;color:#0a0a0a;}th.callout-inner.secondary{background:#ebebeb;border:1px solid #444444;color:#0a0a0a;}th.callout-inner.success{background:#e1faea;border:1px solid #1b9448;color:#fefefe;}th.callout-inner.warning{background:#fff3d9;border:1px solid #996800;color:#fefefe;}th.callout-inner.alert{background:#fce6e2;border:1px solid #b42912;color:#fefefe;}.thumbnail{border:solid 4px #fefefe;box-shadow:0 0 0 1px rgba(10,10,10,0.2);display:inline-block;line-height:0;max-width:100%;transition:box-shadow 200ms ease-out;border-radius:3px;margin-bottom:16px;}.thumbnail:hover,.thumbnail:focus{box-shadow:0 0 6px 1px rgba(33,153,232,0.5);}table.menu{width:580px;}table.menu td.menu-item,table.menu th.menu-item{padding:10px;padding-right:10px;}table.menu td.menu-item a,table.menu th.menu-item a{color:#2199e8;}table.menu.vertical td.menu-item,table.menu.vertical th.menu-item{padding:10px;padding-right:0;display:block;}table.menu.vertical td.menu-item a,table.menu.vertical th.menu-item a{width:100%;}table.menu.vertical td.menu-item table.menu.vertical td.menu-item,table.menu.vertical td.menu-item table.menu.vertical th.menu-item,table.menu.vertical th.menu-item table.menu.vertical td.menu-item,table.menu.vertical th.menu-item table.menu.vertical th.menu-item{padding-left:10px;}table.menu.text-center a{text-align:center;}body.outlook p{display:inline!important;}@media only screen and (max-width: 596px){table.body img{width:auto!important;height:auto!important;}table.body center{min-width:0!important;}table.body .container{width:95%!important;}table.body .columns,table.body .column{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:16px!important;padding-right:16px!important;}table.body .columns .column,table.body .columns .columns,table.body .column .column,table.body .column .columns{padding-left:0!important;padding-right:0!important;}table.body .collapse .columns,table.body .collapse .column{padding-left:0!important;padding-right:0!important;}td.small-1,th.small-1{display:inline-block!important;width:8.33333%!important;}td.small-2,th.small-2{display:inline-block!important;width:16.66667%!important;}td.small-3,th.small-3{display:inline-block!important;width:25%!important;}td.small-4,th.small-4{display:inline-block!important;width:33.33333%!important;}td.small-5,th.small-5{display:inline-block!important;width:41.66667%!important;}td.small-6,th.small-6{display:inline-block!important;width:50%!important;}td.small-7,th.small-7{display:inline-block!important;width:58.33333%!important;}td.small-8,th.small-8{display:inline-block!important;width:66.66667%!important;}td.small-9,th.small-9{display:inline-block!important;width:75%!important;}td.small-10,th.small-10{display:inline-block!important;width:83.33333%!important;}td.small-11,th.small-11{display:inline-block!important;width:91.66667%!important;}td.small-12,th.small-12{display:inline-block!important;width:100%!important;}.columns td.small-12,.column td.small-12,.columns th.small-12,.column th.small-12{display:block!important;width:100%!important;}.body .columns td.small-1,.body .column td.small-1,td.small-1 center,.body .columns th.small-1,.body .column th.small-1,th.small-1 center{display:inline-block!important;width:8.33333%!important;}.body .columns td.small-2,.body .column td.small-2,td.small-2 center,.body .columns th.small-2,.body .column th.small-2,th.small-2 center{display:inline-block!important;width:16.66667%!important;}.body .columns td.small-3,.body .column td.small-3,td.small-3 center,.body .columns th.small-3,.body .column th.small-3,th.small-3 center{display:inline-block!important;width:25%!important;}.body .columns td.small-4,.body .column td.small-4,td.small-4 center,.body .columns th.small-4,.body .column th.small-4,th.small-4 center{display:inline-block!important;width:33.33333%!important;}.body .columns td.small-5,.body .column td.small-5,td.small-5 center,.body .columns th.small-5,.body .column th.small-5,th.small-5 center{display:inline-block!important;width:41.66667%!important;}.body .columns td.small-6,.body .column td.small-6,td.small-6 center,.body .columns th.small-6,.body .column th.small-6,th.small-6 center{display:inline-block!important;width:50%!important;}.body .columns td.small-7,.body .column td.small-7,td.small-7 center,.body .columns th.small-7,.body .column th.small-7,th.small-7 center{display:inline-block!important;width:58.33333%!important;}.body .columns td.small-8,.body .column td.small-8,td.small-8 center,.body .columns th.small-8,.body .column th.small-8,th.small-8 center{display:inline-block!important;width:66.66667%!important;}.body .columns td.small-9,.body .column td.small-9,td.small-9 center,.body .columns th.small-9,.body .column th.small-9,th.small-9 center{display:inline-block!important;width:75%!important;}.body .columns td.small-10,.body .column td.small-10,td.small-10 center,.body .columns th.small-10,.body .column th.small-10,th.small-10 center{display:inline-block!important;width:83.33333%!important;}.body .columns td.small-11,.body .column td.small-11,td.small-11 center,.body .columns th.small-11,.body .column th.small-11,th.small-11 center{display:inline-block!important;width:91.66667%!important;}table.body td.small-offset-1,table.body th.small-offset-1{margin-left:8.33333%!important;Margin-left:8.33333%!important;}table.body td.small-offset-2,table.body th.small-offset-2{margin-left:16.66667%!important;Margin-left:16.66667%!important;}table.body td.small-offset-3,table.body th.small-offset-3{margin-left:25%!important;Margin-left:25%!important;}table.body td.small-offset-4,table.body th.small-offset-4{margin-left:33.33333%!important;Margin-left:33.33333%!important;}table.body td.small-offset-5,table.body th.small-offset-5{margin-left:41.66667%!important;Margin-left:41.66667%!important;}table.body td.small-offset-6,table.body th.small-offset-6{margin-left:50%!important;Margin-left:50%!important;}table.body td.small-offset-7,table.body th.small-offset-7{margin-left:58.33333%!important;Margin-left:58.33333%!important;}table.body td.small-offset-8,table.body th.small-offset-8{margin-left:66.66667%!important;Margin-left:66.66667%!important;}table.body td.small-offset-9,table.body th.small-offset-9{margin-left:75%!important;Margin-left:75%!important;}table.body td.small-offset-10,table.body th.small-offset-10{margin-left:83.33333%!important;Margin-left:83.33333%!important;}table.body td.small-offset-11,table.body th.small-offset-11{margin-left:91.66667%!important;Margin-left:91.66667%!important;}table.body table.columns td.expander,table.body table.columns th.expander{display:none!important;}table.body .right-text-pad,table.body .text-pad-right{padding-left:10px!important;}table.body .left-text-pad,table.body .text-pad-left{padding-right:10px!important;}table.menu{width:100%!important;}table.menu td,table.menu th{width:auto!important;display:inline-block!important;}table.menu.vertical td,table.menu.vertical th,table.menu.small-vertical td,table.menu.small-vertical th{display:block!important;}table.button.expand{width:100%!important;}}</style><style>.header{background:#8a8a8a;}.header p{color:#ffffff;margin:0;}.header .columns{padding-bottom:0;}.header .container{background:#8a8a8a;padding-top:16px;padding-bottom:16px;}.header .container td{padding-top:16px;padding-bottom:16px;}</style><body><table class="body" data-made-with-foundation=""><tr><td class="center" align="center" valign="top"><center data-parsed=""><div class="header text-center" align="center"><table class="container"><tbody><tr><td><table class="row collapse"><tbody><tr><th class="small-6 large-6 columns first"><table><tr><th> <img src="http://placehold.it/150x30/663399"> </th></tr></table></th><th class="small-6 large-6 columns last"><table><tr><th><p class="text-right">BASIC</p></th></tr></table></th></tr></tbody></table></td></tr></tbody></table></div><table class="container text-center"><tbody><tr><td><table class="row"><tbody><tr><th class="small-12 large-12 columns first last"><table><tr><th><center data-parsed=""> <img src="http://placehold.it/200x50" align="center" class="text-center"> </center></th><th class="expander"></th></tr></table></th></tr></tbody></table><table class="row"><tbody><tr><th class="small-12 large-12 columns first last"><table><tr><th><h1>Hi, Elijah Baily</h1><p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi impedit sapiente delectus molestias quia.</p><center data-parsed=""> <img src="http://placehold.it/570x300" alt="" align="center" class="text-center"> </center><table class="callout"><tr><td class="callout-inner primary"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam assumenda, praesentium qui vitae voluptate dolores. <a href="#">Click it!</a></p></td><td class="expander"></td></tr></table><table class="row"><tbody><tr><th class="small-12 large-7 columns first"><table><tr><th><h3>Hello, Han Fastolfe</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam nobis velit, aliquid pariatur at fugit. Omnis at quae, libero iusto quisquam animi blanditiis neque, alias minima corporis, ab in explicabo?</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime dignissimos voluptas minus, cupiditate voluptatem, voluptatum iste molestiae consectetur temporibus quae dolore nam possimus reprehenderitblanditiis laborum iusto sit. Perspiciatis, dolor.</p><table class="callout"><tr><td class="callout-inner secondary"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa quas optio totam quidem, placeat sunt, sit iusto fugit. Harum omnis deleniti enim nihil iure, quis laudantium veniam velit animi debitis.<a href="#">Click It!</a> </td><td class="expander"></td></tr></table><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores minus eius amet alias odit accusantium, fugit perspiciatis nulla suscipit nisi. Laborum aliquid, voluptatum consectetur fugiat maxime architectoenim molestias aperiam!</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex eveniet veritatis, magnam ipsam et vero necessitatibus. Deserunt facilis impedit, adipisci illo laboriosam assumenda fugiat dolorum nam odio aliquid,sit est.</p><table class="button expand"><tr><td><table><tr><td><center data-parsed=""><a href="#" align="center" class="text-center">Click Me!</a></center></td><td class="expander"></td></tr></table></td></tr></table></th></tr></table></th><th class="small-12 large-5 columns last"><table><tr><th><table class="callout"><tr><td class="callout-inner secondary"><h5>Header</h5><p class="lead">Sub-header</p><table class="menu vertical"><tr><td><table><tr><th class="menu-item float-center"><a href="#">Just a Plain Link &#xBB;</a></th><th class="menu-item float-center"><a href="#">Just a Plain Link &#xBB;</a></th><th class="menu-item float-center"><a href="#">Just a Plain Link &#xBB;</a></th><th class="menu-item float-center"><a href="#">Just a Plain Link &#xBB;</a></th><th class="menu-item float-center"><a href="#">Just a Plain Link &#xBB;</a></th><th class="menu-item float-center"><a href="#">Just a Plain Link &#xBB;</a></th><th class="menu-item float-center"><a href="#">Just a Plain Link &#xBB;</a></th></tr></table></td></tr></table></td><td class="expander"></td></tr></table><table class="callout"><tr><td class="callout-inner secondary"><h5>Contact Info:</h5><table class="button facebook expand"><tr><td><table><tr><td><center data-parsed=""><a href="#" align="center" class="text-center">Facebook</a></center></td><td class="expander"></td></tr></table></td></tr></table><table class="button twitter expand"><tr><td><table><tr><td><center data-parsed=""><a href="#" align="center" class="text-center">Twitter</a></center></td><td class="expander"></td></tr></table></td></tr></table><table class="button google expand"><tr><td><table><tr><td><center data-parsed=""><a href="#" align="center" class="text-center">Google+</a></center></td><td class="expander"></td></tr></table></td></tr></table><p>Phone: 408-341-0600</p><p>Email: <a href="mailto:foundation@zurb.com">foundation@zurb.com</a></p></td><td class="expander"></td></tr></table></th></tr></table></th></tr></tbody></table><center data-parsed=""><table class="menu text-center" align="center"><tr><td><table><tr><th class="menu-item float-center"><a href="#">Terms</a></th><th class="menu-item float-center"><a href="#">Privacy</a></th><th class="menu-item float-center"><a href="#">Unsubscribe</a></th></tr></table></td></tr></table></center></th></tr></table></th></tr></tbody></table></td></tr></tbody></table></center></td></tr></table></body></html>';
	console.log("links = " + extractLinks(content));
}

function test5(){
	utils.init();

	var webhookJobPayload = {};
	var emailId = "nitin@webklipper.com";
	
	webhookJobPayload['x-smtpapi'] = {
		"unique_args": {
			"trackerId": "jm7xZss5p8wM6GYZ48mOiCeKZ3f0Z/EG0QKHFALcAX/O9aSf1r5qqcDc4KsygqHirpQ9+lYs7c+JU6znu+5141f32ZT6nRCHIdQgD5CTCe5QUoYmqN89hbieqte2eJsGOtsTW3fQ0FYlgraPE9H/88IKLacxmhIxPsEpNReF5kFMRcX9ojMUSDfqNS/YjzVKdpMt7CIyoVftbxA95kK/mBr6L+xyfnl8mdOCIVKEDTlNQpmaCA6sGA=="
		}
	};
	webhookJobPayload['links'] = ['http://google.com'];
	webhookJobPayload.stage = "processed";

	var i = 1;
	while(webhookJobPayload.stage != "end") {
		var webhookResponseDetails = utils.getWebhookResponseDetails(webhookJobPayload, emailId);
		webhookJobPayload.stage = webhookResponseDetails.stage;
		fs.appendFileSync('./webhookPayloads.txt', "\n\n" + JSON.stringify(webhookResponseDetails), 'utf-8');
		console.log(i++);
	}
}

test5();
