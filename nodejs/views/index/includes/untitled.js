function scroll($ele){
	$('html, body').animate({
	    scrollTop: $ele.offset().top
	}, 1000);
}

var idx = 10;
setInterval(function(){
	if(idx == 10) idx = 1;
	else idx = 10;
	
	scroll($("h2:eq("+idx+")"));
}, 1100);




var hSizeMap = {
	"H1": "1.2em",
	"H2": "1.1em",
	"H3": "1.0em",
	"H4": "0.9em",
	"H5": "0.8em",
	"H6": "0.7em"
};

var hPadMap = {
	"H1": "5px",
	"H2": "8px",
	"H3": "11px",
	"H4": "14px",
	"H5": "17px",
	"H6": "20px"
};

function scroll($ele){
	$('html, body').animate({
	    scrollTop: $ele.offset().top
	}, 1000);
}

var $body = $("body");

var $c = $('<div id="toc-container"></div>');
$c.height($(window).height());
$c.width($(window).width()/5);
$c.css({
	'border': '2px solid #cccccc',
	'border-left': 'none',
	'position': 'fixed',
	'top':'0px',
	'overflow-y': 'scroll',
	'left': '0px',
	'background-color': '#000000',
	'z-index': 9999999
});

$body.append($c);

var $hss = $("h1, h2, h3");
$hss.each(function(){
	var tagName = $(this).prop("tagName");
	var textSize = hSizeMap[tagName];
	var leftPad = hSizeMap[tagName];

	var $h = $('<div></div>');
	$h.css({
		'text-align': 'left',
		'padding-left': leftPad,
		'color': '#333333',
		'border-bottom': '1px solid #aaaaaa',
		'width': '100%',
		'height': '50px',
		'line-height': '50px',
		'cursor': 'pointer'
	});

	var $s = $('<span></span>');
	$s.css({
		'display': 'inline-block',
  		'vertical-align': 'middle',
  		'line-height': 'normal',
  		'color': '#cccccc',
  		'font-size': textSize
	});

	
	$s.text($(this).text());
	$h.append($s);

	var $that = $(this);
	$h.click(function(){
		scroll($that);
	});

	$c.append($h);
});
