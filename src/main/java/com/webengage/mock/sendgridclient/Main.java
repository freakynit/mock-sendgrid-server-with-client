package com.webengage.mock.sendgridclient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import org.apache.log4j.Logger;

import java.util.Random;

public class Main {
    private static Logger log = Logger.getLogger(Main.class);
    private static Random random = new Random();
    private static Gson restGson = new GsonBuilder().serializeNulls().enableComplexMapKeySerialization().create();
    private static Main main;

    public static void main(String[] args){
        main = new Main();
        main.sendMail();
    }


    private void sendMail(){
        //SendGrid sendgrid = new SendGrid("freakynit", "admin123");
        SendGrid sendgrid = new SendGrid("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");
//        sendgrid.setUrl("http://ec2-54-189-200-2.us-west-2.compute.amazonaws.com:9002");
//        sendgrid.setEndpoint("/api/mail.send.json");
        sendgrid.setUrl("http://localhost:9002");
        sendgrid.setEndpoint("/api/mail.send.json");

        SendGrid.Email email = new SendGrid.Email();
        email.getSMTPAPI().addUniqueArg("trackerId", "jm7xZss5p8wM6GYZ48mOiCeKZ3f0Z/EG0QKHFALcAX/O9aSf1r5qqcDc4KsygqHirpQ9+lYs7c+JU6znu+5141f32ZT6nRCHIdQgD5CTCe5QUoYmqN89hbieqte2eJsGOtsTW3fQ0FYlgraPE9H/88IKLacxmhIxPsEpNReF5kFMRcX9ojMUSDfqNS/YjzVKdpMt7CIyoVftbxA95kK/mBr6L+xyfnl8mdOCIVKEDTlNQpmaCA6sGA==");

        email.addTo("nitin@webklipper.com");
        email.addTo("nitinbansal85@gmail.com");
        email.addTo("nitinbansal85.2@gmail.com");

        email.addToName("nitin-webklipper");
        email.addToName("nitinbansal85");
        email.addToName("nitinbansal85.2");

        email.setFrom("niitn@webklipper.com");
        email.setHtml("<h2>Hello There. Go to <a href='http://webengage.com'>WebEngage</a></h2>");
        email.setReplyTo("nitin@webklipper.com");

        email.addCc("nitinbansal85.3@gmail.com");
        email.addBcc("nitinbansal85.4@gmail.com");
//        try {
//            email.addAttachment("pom", new File("pom.xml"));
//        } catch (IOException e) {
//            log.error(e);
//        }


        for(int i = 0; i < 5; i++){
            email.setSubject("Hello World");
//            if(random.nextInt(5) > 2) {
//                email.setSubject("Hello World");
//            }

            try {
                SendGrid.Response response = sendgrid.send(email);
                //log.info(response.getMessage());
                log.warn(restGson.toJson(response, SendGrid.Response.class));

                Thread.currentThread().sleep(2000);
            }
            catch (SendGridException e) {
                log.error(e.getMessage());
                break;
            } catch (InterruptedException e) {
                log.error(e);
            }
        }
    }
}
